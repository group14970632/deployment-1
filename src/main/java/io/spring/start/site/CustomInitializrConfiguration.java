package io.spring.start.site;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import io.spring.initializr.metadata.InitializrMetadataBuilder;
import io.spring.initializr.metadata.InitializrProperties;
import io.spring.initializr.web.support.DefaultInitializrMetadataProvider;
import io.spring.initializr.web.support.InitializrMetadataUpdateStrategy;

@Configuration
@EnableConfigurationProperties(CustomInitializrProperties.class)
public class CustomInitializrConfiguration {

	@Bean
	public DefaultInitializrMetadataProvider customInitializrMetadataProvider(InitializrProperties initializrProperties,
			CustomInitializrProperties customInitializrProperties,
			InitializrMetadataUpdateStrategy initializrMetadataUpdateStrategy) {
		return new DefaultInitializrMetadataProvider(
				InitializrMetadataBuilder.fromInitializrProperties(customInitializrProperties.getInitializr())
					.withInitializrProperties(initializrProperties, true)
					.build(),
				initializrMetadataUpdateStrategy);
	}

}

@ConfigurationProperties("custom")
class CustomInitializrProperties {

	@NestedConfigurationProperty
	private InitializrProperties initializr = new InitializrProperties();

	public InitializrProperties getInitializr() {
		return initializr;
	}

}
