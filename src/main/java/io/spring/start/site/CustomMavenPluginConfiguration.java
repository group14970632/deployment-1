package io.spring.start.site;

import io.spring.initializr.generator.buildsystem.Build;
import io.spring.initializr.generator.buildsystem.maven.MavenBuild;
import io.spring.initializr.generator.condition.ConditionalOnRequestedDependency;
import io.spring.initializr.generator.project.ProjectGenerationConfiguration;
import io.spring.initializr.generator.spring.build.BuildCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@ProjectGenerationConfiguration
@ConditionalOnRequestedDependency("custom-maven-plugin")
public class CustomMavenPluginConfiguration {

	@Bean
	public BuildCustomizer<MavenBuild> addCustomPlugin() {
		return build -> {
			// Configure the Maven plugin and execution here
			String groupId = "your-group-id";
			String artifactId = "your-artifact-id";
			String version = "your-version";

			build.plugins().add(groupId, artifactId);
		};
	}

	@Bean
	@Primary
	public BuildCustomizer<Build> removeCustomPluginDependency() {
		return build -> {
			build.dependencies().remove("custom-maven-plugin");
		};
	}

}

// package io.spring.start.site;
//
// import java.util.Objects;
// import java.util.stream.Stream;
//
// import org.springframework.context.annotation.Bean;
// import org.springframework.core.Ordered;
//
// import io.spring.initializr.generator.buildsystem.Build;
// import io.spring.initializr.generator.buildsystem.maven.MavenBuild;
// import io.spring.initializr.generator.condition.ConditionalOnRequestedDependency;
// import io.spring.initializr.generator.project.ProjectGenerationConfiguration;
// import io.spring.initializr.generator.spring.build.BuildCustomizer;
// import io.spring.initializr.generator.buildsystem.Dependency;
//
// @ProjectGenerationConfiguration
// @ConditionalOnRequestedDependency("custom-maven-plugin")
// public class CustomMavenPluginConfiguration {
//
// @Bean
// public BuildCustomizer<MavenBuild> customPluginConfigurer() {
// return build -> {
// String customMavenPlugin = (build.dependencies().ids())
//// .stream()
// .filter(id -> Objects.equals(id, "custom-maven-plugin"))
// .findFirst()
// .orElseThrow(() -> new IllegalStateException("custom-maven-plugin not found"));
//
// Dependency dependency = build.dependencies().get(customMavenPlugin);
// if (dependency != null) {
// String groupId = dependency.getGroupId();
// String artifactId = dependency.getArtifactId();
// String version = dependency.getVersion().getValue();
//
// build.plugin(groupId, artifactId, version)
// .execution("my-execution", it -> {
// it.goal("scan").configuration(config -> config.add("failOnSeverity", "MAJOR"));
// });
//
// } else {
// throw new IllegalStateException("custom-maven-plugin not found");
// }
// };
// }
//
// @Bean
// public BuildCustomizer<Build> customPluginDependencyRemoval() {
// return new BuildCustomizer<Build>() {
// @Override
// public void customize(Build build) {
// build.dependencies().remove("custom-maven-plugin");
// }
//
// @Override
// public int getOrder() {
// return Ordered.LOWEST_PRECEDENCE;
// }
// };
// }
// }
